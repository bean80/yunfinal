package com.yunfinal.test;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

/***
 * YunFinalConfig
 * 
 *  项目WebSocket需要使用 JDK8＋　的版本
 * 
 * @author dufuzhong
 *
 */
public class YunFinalConfig extends JFinalConfig {
	public static void main(String[] args) {
	    UndertowServer.create(YunFinalConfig.class)
	            .configWeb(builder -> {
	                builder.addWebSocketEndpoint(WebSocketController.class);
	            })
	            .setPort(8080)
	            .setDevMode(true)
	            .start();
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/" , IndexController.class);
	}
	
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
	}

	@Override
	public void configEngine(Engine me) {
		me.setDevMode(true);
	}

	@Override
	public void configHandler(Handlers me) {
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configPlugin(Plugins me) {
	}


}
